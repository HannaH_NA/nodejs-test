const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const tweet = mongoose.Schema({
	title               : { type: String, required: true },
    author              : { type: Schema.Types.ObjectId, ref: 'User', required:true },
	subject            : { type: String, required: false },
	imageUrl            : { type: String, required: false },
	createdAt   		: { type: Number, required: false },
    updatedAt   		: { type: Number, required: false },
});

tweet.pre('save', function(next) {
	now = new Date().getTime();
	if (!this.createdAt ) {
		this.createdAt = now;
    }
    this.updatedAt = now;
	next();
});


module.exports = mongoose.model('Tweet', tweet);