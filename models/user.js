const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const user = mongoose.Schema({
	dob                 : { type: Date, required: true },
	username            : { type: String, required: true },
	email               : { type: String, required: true, unique: true },
	tweet				: [{ type: Schema.Types.ObjectId, ref: 'Tweet' }],
	password            : { type: String, required: true  },
	phone               : { type: Number, required: true  },
	createdAt   		: { type: Number, required: false },
    updatedAt   		: { type: Number, required: false },
});

user.pre('save', function(next) {
	now = new Date().getTime();
	if (!this.createdAt ) {
		this.createdAt = now;
    }
    this.updatedAt = now;
	next();
});


module.exports = mongoose.model('User', user);