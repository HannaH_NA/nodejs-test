const express = require("express");
const path = require("path");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const logger = require('morgan');
const cors = require('cors'); 

const db = require('./config/config').get(process.env.NODE_ENV);

const indexRouter = require("./routes/user");
const apiRouter = require("./routes/api");

const app = express();
const port = 3000;

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "public")));


mongoose.Promise = global.Promise;
mongoose.connect(db.DATABASE, { useNewUrlParser: true, useUnifiedTopology: true }, function (err) {
  if (err) console.log(err);
  console.log("database is connected");
});

// Body Parser Middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


app.use("/", indexRouter);
app.use("/api", apiRouter);

app.listen(port, () => {
  console.log("Server listening on port " + port);
});


