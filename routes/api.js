var express = require("express");
const { response } = require("../app");
var router = express.Router();

const path = require("path");


const authHelper = require("../helpers/auth");

// router.post("/signup", adminController.signup);
// router.post("/signin", adminController.signin);

const userController  = require('../controllers/user');
const tweetController  = require('../controllers/tweet');

router.post('/register', userController.signup);
router.post('/login', userController.signin);
router.post('/addTweet', authHelper.authCheck, tweetController.addTweet);
router.get('/getTweets', authHelper.authCheck, tweetController.getTweets);
router.get('/getTweet/:id', authHelper.authCheck, tweetController.getTweet);
router.delete('/deleteTweet/:id/:authorId', authHelper.authCheck, tweetController.deleteTweet);
router.put('/editTweet/:id/:authorId', authHelper.authCheck, tweetController.editTweet);


module.exports = router;
