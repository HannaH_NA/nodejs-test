const Tweet = require('../models/tweet');

const TweetController = {

	/**
	 * addTweet function
	 * @param {object} req request object form current http request
	 * @param {object} res responce object form current http request
	 * @returns {object} responce object with status code , responce success/error data
	 */
    addTweet: function (req, res) {
        var data = { title: '',subject:'' }
        data = { ...data, ...req.body }
        var validationErrors = [];
        Object.keys(data).forEach(function (key) {
            if (!data[key]) {
                validationErrors.push({ [key]: [key] + ' is mandatory' })
            }
        });

        if (!validationErrors.length) {
            const tweet = new Tweet({ ...data, author: req.headers.currentUser._id })

            tweet.save().then((response) => {

                return res.status(200).json({
                    success: true,
                    data: response
                });

            }).catch((error) => {
                return res.status(422).json({
                    success: false,
                    error: {
                        types: 'Failure',
                        messages: 'failed adding data to db'
                    }
                });

            })

        } else {
            return res.status(422).json({
                success: false,
                error: {
                    types: 'Validation Error',
                    messages: validationErrors
                }
            });
        }
    },

	/**
	 * getTweet function
	 * @param {object} req request object form current http request
	 * @param {object} res responce object form current http request
	 * @returns {object} responce object with status code , responce success/error data
	 */
    getTweets: function (req, res) {
        Tweet.find({}).
        exec(function (error, tweets) {
            if(!error){
                return res.status(200).json({
                    success: true,
                    data: tweets
                });

            } else {
                return res.status(422).json({
                    success: false,
                    error: {
                        types: 'Failure',
                        messages: 'failed retriving data to db'
                    }
                });

            }
        })
    },
    getTweet: (req, res) => {
        Tweet.find({author:req.params.id})
          .then((result) => {
            console.log("view data", result);
            return res.status(200).json({
              success: true,
              result: result,
            });
          })
          .catch((err) => {
            console.log("error" + err);
            return res.status(422).json({
              success: false,
              error: err,
            });
          });
      },

      editTweet: (req, res) => {
        Tweet.findOne({$and:[
            { _id: req.params.id },{author:req.params.authorId}
          ]})
          .then((response) => {
          if (response != null) {
            Tweet.findByIdAndUpdate(req.params.id, { $set: req.body })
              .then((result) => {
                if (result) {
                  return res.status(200).json({
                    success: true,
                    result: "updated ",
                  });
                } else {
                  return res.status(422).json({
                    success: false,
                    result: "error",
                  });
                }
              })
              .catch((error) => {
                return res.status(422).json({
                  success: false,
                  result: error,
                });
              });
          } else {
            return res.status(422).json({
              success: false,
              error: "already exist ",
            });
          }
        });
      },
      deleteTweet: (req, res) => {
        Tweet.findOne({$and:[
            { _id: req.params.id },{author:req.params.authorId}
          ]})
          .then((response) => {
            if (response != null) {
              Tweet.remove({_id:req.params.id})
                .then((result) => {
                  return res.status(200).json({
                    success: true,
                    result: "Deleted ",
                  });
                })
                .catch((error) => {
                  return res.status(422).json({
                    success: false,
                    result: error,
                  });
                });
            } else {
              return res.status(422).json({
                success: false,
                result: "Tweet not exist ",
              });
            }
          })
          .catch((error) => {
            return res.status(200).json({
              success: false,
              result: error,
            });
          });
      }
};

module.exports = TweetController;