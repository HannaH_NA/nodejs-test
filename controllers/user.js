const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const mongoose = require("mongoose");
const passwordValidator = require("password-validator");
const emailValidator = require("email-validator");
const User = require("../models/user");

var password = new passwordValidator();
password
  .is()
  .min(8) // Minimum length 8
  .is()
  .max(16) // Maximum length 100
  .has()
  .uppercase() // Must have uppercase letters
  .has()
  .lowercase() // Must have lowercase letters
  .has()
  .digits(2) // Must have at least 2 digits
  .has()
  .not()
  .spaces(); // Should not have spaces

const Auth = {
  signin: (req, res) => {
    var data = { email: "", password: "" };
    data = { ...data, ...req.body };
    User.findOne(
      {
        email: data.email,
      },
      (error, user) => {
        if (!error) {
          if (user != null) {
            bcrypt.compare(data.password, user.password, (err, result) => {
              if (!err && result) {
                const JWTToken = jwt.sign(
                  {
                    _id: user._id
                  },
                  "secretKey",
                  { expiresIn: '3h' }
                );
                res.cookie("test", JWTToken, {
                  maxAge: 900000,
                  httpOnly: true,
                });

                return res.status(200).json({
                  success: true,
                  data: {
                    _id: user._id,
                    email: user.email,
                    username: user.username,
                    dob: user.dob,
                    token: JWTToken,
                  },
                });
              } else {
                res.status(422).json({
                  success: false,
                  error: {
                    message: [{ password: "Invalid password" }],
                  },
                });
              }
            });
          } else {
            return res.status(422).json({
              success: false,
              error: {
                types: "Validation Error",
                messages: [{ email: "Email is not registered" }],
              },
            });
          }
        } else {
          return res.status(422).json({
            success: false,
            error: {
              types: "Failure",
              messages: "findOne in db failed",
            },
          });
        }
      }
    );
  },

  signup: async (req, res) => {


    // Validate against a password string
    var data = {
      email: "",
      phone: "",
      username: "",
      password: "",
    };
    data = { ...data, ...req.body };
    var validationErrors = [];

    Object.keys(data).forEach(function (key) {
      if (!data[key]) {
        validationErrors.push({ [key]: [key] + " is mandatory" });
      }
    });

    if (!validationErrors.length) {
      if (password.validate(data.password) == false) {
        return res.status(422).json({
          success: false,
          error: {
            types: "Validation error",
            messages: "Password Format is wrong",
          },
        });
      } else {
        if (emailValidator.validate(data.email) == false) {
          return res.status(422).json({
            success: false,
            error: {
              types: "Validation error",
              messages: "Email Format is wrong",
            },
          });
        } else {
              bcrypt.hash(data.password, 10, (error, hash) => {
                if (!error) {
                  User.findOne({email: data.email},                   
                    (error, existingUser) => {
                      if (!error) {
                        User.findOne(
                          { phone: data.phone },
                          (error, phone) => {
                            if (!error && phone == null) {
                              if (existingUser == null) {
                                const myData = new User({
                                  ...data,
                                  password: hash
                                });

                                myData
                                  .save()
                                  .then((response) => {
                                    const JWTToken = jwt.sign(
                                      {
                                        _id: response._id
                                      },
                                      "secretKey",
                                      { expiresIn: 1 * 24 * 60 * 60 * 1000 }
                                    );

                                    console.log(
                                      "data save to database" + myData
                                    );

                                    return res.status(200).json({
                                      success: true,
                                      data: {
                                        _id: response._id,
                                        email: response.email,
                                        phone: response.phone,
                                        password: response.password,
                                        token: JWTToken,
                                      
                                      },
                                    });
                                  })
                                  .catch((error) => {
                                    return res.status(422).json({
                                      success: false,
                                      error: "failed adding data to db"
                                    });
                                  });
                              } else {
                                return res.status(422).json({
                                  success: false,
                                  error: "Email Already Exists" 
                                });
                              }
                            } else {
                              return res.status(422).json({
                                success: false,
                                error: "Phone Number Already Exists" 
                              });
                            }
                          }
                        );
                      } else {
                        return res.status(422).json({
                          success: false,
                          error: "findOne in db failed"
                        });
                      }
                    }
                  );
                } else {
                  return res.status(422).json({
                    success: false,
                    error: "hashing password failed"
                  });
                }
              });
          }
        }
      // }
    } else {
      return res.status(422).json({
        success: false,
        error: {
          types: "Validation Error",
          messages: validationErrors,
        },
      });
    }
  },
};
module.exports = Auth;
